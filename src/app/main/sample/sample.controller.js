(function ()
{
    'use strict';

    angular
        .module('app.sample')
        .controller('SampleController',  SampleController);

    /** @ngInject */
    function SampleController($scope, SampleData)
    {
        var vm  = this;

        // Data
        vm.data = SampleData.data;

/// ===== colunm chart
        $scope.columnChartObject = {};    
        $scope.columnChartObject.type = "ColumnChart";
        $scope.columnChartObject.data = 
        {"cols": [
        {id: "t", label: "Year", type: "string"},
        {id: "s", label: "Price", type: "number"}
        ], "rows": vm.data };
        $scope.columnChartObject.options = {
            'title': 'Column Chart'
        };

/// ===== pie chart
        $scope.pieChartObject = {};        
        $scope.pieChartObject.type = "PieChart";        
        $scope.pieChartObject.data = 
        {"cols": [
        {id: "t", label: "Year", type: "string"},
        {id: "s", label: "Price", type: "number"}
        ], "rows": vm.data};
        $scope.pieChartObject.options = {
            'title': 'Pie Chart'
        };
        //////////
    }
})();
